package si.derp.ojpp.lpp_gtfs_rt;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.apache.commons.cli.*;
import org.onebusaway.guice.jsr250.LifecycleService;
import org.onebusway.gtfs_realtime.exporter.TripUpdatesFileWriter;
import org.onebusway.gtfs_realtime.exporter.TripUpdatesServlet;
import org.onebusway.gtfs_realtime.exporter.VehiclePositionsFileWriter;
import org.onebusway.gtfs_realtime.exporter.VehiclePositionsServlet;

import javax.inject.Inject;
import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class GTFSRTProxyMain {

    private static final String ARG_HELP = "help";
    private static final String ARG_LPP_API_KEY = "lppApiKey";
    private static final String ARG_TRIP_UPDATES_PATH = "tripUpdatesPath";
    private static final String ARG_TRIP_UPDATES_URL = "tripUpdatesUrl";
    private static final String ARG_VEHICLE_POSITIONS_PATH = "vehiclePositionsPath";
    private static final String ARG_VEHICLE_POSITIONS_URL = "vehiclePositionsUrl";

    public static void main(String[] args) throws Exception {
        GTFSRTProxyMain m = new GTFSRTProxyMain();
        m.run(args);
    }

    static String LPP_API_KEY;
    static boolean ARRIVALS_ENABLED = false;

    public GTFSRTProviderImpl _provider;

    public LifecycleService _lifecycleService;

    @Inject
    public void setProvider(GTFSRTProviderImpl provider) {
        _provider = provider;
    }

    @Inject
    public void setLifecycleService(LifecycleService lifecycleService) {
        _lifecycleService = lifecycleService;
    }

    public void run(String[] args) throws Exception {

        Options options = new Options();
        buildOptions(options);
        DefaultParser parser = new DefaultParser();
        CommandLine cli = parser.parse(options, args);

        if (args.length == 0 || cli.hasOption(ARG_HELP)) {
            printHelp(options);
            System.exit(-1);
        }

        Set<Module> modules = new HashSet<>();
        GTFSRTProducerModule.addModuleAndDependencies(modules);

        Injector injector = Guice.createInjector(modules);
        injector.injectMembers(this);

        // TODO: set API key as required option
        LPP_API_KEY = cli.getOptionValue(ARG_LPP_API_KEY);

        if (cli.hasOption(ARG_TRIP_UPDATES_URL)) {
            URL url = new URL(cli.getOptionValue(ARG_TRIP_UPDATES_URL));
            TripUpdatesServlet servlet = injector.getInstance(TripUpdatesServlet.class);
            servlet.setUrl(url);
            ARRIVALS_ENABLED = true;
        }
        if (cli.hasOption(ARG_TRIP_UPDATES_PATH)) {
            File path = new File(cli.getOptionValue(ARG_TRIP_UPDATES_PATH));
            TripUpdatesFileWriter writer = injector.getInstance(TripUpdatesFileWriter.class);
            writer.setPath(path);
            ARRIVALS_ENABLED = true;
        }

        if (cli.hasOption(ARG_VEHICLE_POSITIONS_URL)) {
            URL url = new URL(cli.getOptionValue(ARG_VEHICLE_POSITIONS_URL));
            VehiclePositionsServlet servlet = injector.getInstance(VehiclePositionsServlet.class);
            servlet.setUrl(url);
        }
        if (cli.hasOption(ARG_VEHICLE_POSITIONS_PATH)) {
            File path = new File(cli.getOptionValue(ARG_VEHICLE_POSITIONS_PATH));
            VehiclePositionsFileWriter writer = injector.getInstance(VehiclePositionsFileWriter.class);
            writer.setPath(path);
        }

        _lifecycleService.start();
    }

    private void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setLongOptPrefix("--");
        formatter.setLongOptSeparator("=");
        formatter.printHelp("java -jar lpp_gtfs_rt.jar", options);
    }

    protected void buildOptions(Options options) {
        options.addOption("h", ARG_HELP, false, "Show help");
        options.addOption(Option.builder(null).longOpt(ARG_LPP_API_KEY).desc("LPP API key").hasArg().required().build());
//        options.addOption(null, ARG_LPP_API_KEY, true, "LPP API key");
        options.addOption(null, ARG_TRIP_UPDATES_PATH, true, "write GTFS-realtime trip updates to the specified path");
        options.addOption(null, ARG_TRIP_UPDATES_URL, true, "share GTFS-realtime trip updates at the specified URL (eg. \"http://localhost:8080/trip-updates\")");
        options.addOption(null, ARG_VEHICLE_POSITIONS_PATH, true, "write GTFS-realtime vehicle positions to the specified path");
        options.addOption(null, ARG_VEHICLE_POSITIONS_URL, true, "share GTFS-realtime vehicle positions at the specified URL (eg. \"http://localhost:8080/vehicle-positions\")");
    }
}
