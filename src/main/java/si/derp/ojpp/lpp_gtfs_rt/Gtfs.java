package si.derp.ojpp.lpp_gtfs_rt;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static si.derp.ojpp.lpp_gtfs_rt.Gtfs._log;

public class Gtfs implements GtfsDataUpdater.OnGtfsData {
    private GtfsData data;

    static final Logger _log = LoggerFactory.getLogger(GTFSRTProviderImpl.class);



//    public static void main(String[] args) throws Exception {
//        new Gtfs();
//        while (true) {
//        }
//    }

    public Gtfs() {
//        try {
//            new Thread(new GtfsDataUpdater(this)).start();
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onGtfsData(GtfsData data) {
        this.data = data;
    }

    boolean isReady() {
        return data != null;
    }

    String getTripId(String subRouteId, String stationCode, LocalDateTime arrivalTime) {
        LocalDateTime lDate = LocalDateTime.now(ZoneId.of("Europe/Ljubljana"));
        String serviceId = data.date2ServiceId.get(lDate.format(GtfsData.Service.dateFormat));
        if (serviceId == null) {
            _log.debug("serviceId is null for subRouteId: " + subRouteId + " stationCode: " + stationCode + " arrivalTime: " + arrivalTime);
            return null;
        }
        GtfsData.Service service = data.services.get(serviceId);
        if (service == null) {
            _log.debug("service is null for subRouteId: " + subRouteId + " stationCode: " + stationCode + " arrivalTime: " + arrivalTime);
            return null;
        }
        subRouteId = subRouteId.toLowerCase();

        GtfsData.Route route = service.routes.get(data.subroute2route.get(subRouteId));
        if (route == null) {
            _log.debug("route is null for subRouteId: " + subRouteId + " stationCode: " + stationCode + " arrivalTime: " + arrivalTime);
            return null;
        }

        GtfsData.SubrouteTrips trips = route.subRouteId2Trips.get(subRouteId);
        if (trips == null) {
            _log.debug("trips is null for subRouteId: " + subRouteId + " stationCode: " + stationCode + " arrivalTime: " + arrivalTime);
            return null;
        }
        return trips.getTripId(stationCode, arrivalTime);
    }
}

class GtfsDataUpdater implements Runnable {
    private final URL gtfsUrl = new URL("https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/lpp_gtfs.zip?job=LPP");
    private final long updateIntervalMs = 30 * 60 * 1000;
    private boolean _exit;
    private final OnGtfsData callback;

    GtfsDataUpdater(OnGtfsData onGtfsData) throws MalformedURLException {
        this.callback = onGtfsData;
    }

    interface OnGtfsData {
        void onGtfsData(GtfsData data);
    }

    @Override
    public void run() {
//        while (true) {
//            if (_exit) return;
//            doUpdate();
//            try {
//                Thread.sleep(updateIntervalMs);
//            } catch (InterruptedException e) {
//                return;
//            }
//        }
        doUpdate();

    }

    private void doUpdate() {
        ZipInputStream stream;
        try {
            // stream = new ZipInputStream(new FileInputStream("feed.zip"));
            stream = new ZipInputStream(gtfsUrl.openStream());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        try {
            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            GtfsData data = new GtfsData();
            ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {
                try {
                    data.consumeFile(entry.getName(), reader);
                } catch (CsvValidationException e) {
                    e.printStackTrace();
                }
            }
            data.onLoaded();
            callback.onGtfsData(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void exit() {
        _exit = true;
    }
}

class GtfsData {
    final HashMap<String, String> date2ServiceId = new HashMap<>();
    final HashMap<String, Service> services = new HashMap<>();
    final HashMap<String, String> stopCode2StopId = new HashMap<>();
    final HashMap<String, Integer> tripId2StartTime = new HashMap<>();
    final HashMap<String, HashMap<String, ArrayList<Integer>>> tripId2StopId2ArrivalTimes = new HashMap<>();
    final HashMap<String, HashSet<String>> tripId2StopIds = new HashMap<>();
    final HashMap<String, String> subroute2route = new HashMap<>();

    static class Service {
        static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");

        HashMap<String, Route> routes = new HashMap<>();
    }

    static class Route {
        HashMap<String, SubrouteTrips> subRouteId2Trips = new HashMap<>();
        HashMap<String, Trip> trips = new HashMap<>();
    }

    class SubrouteTrips {
        TreeMap<Integer, Trip> tripStart2Trip = new TreeMap<>();
        ArrayList<Trip> trips = new ArrayList<>();
        HashMap<String, TreeMap<Integer, Trip>> stopId2ArrivalTime2TripId = new HashMap<>();

        public String getTripId(String stationCode, LocalDateTime arrivalTime) {
            int date = arrivalTime.getHour() * 60 * 60 + arrivalTime.getMinute() * 60 + arrivalTime.getSecond();
            String stopId = stopCode2StopId.get(stationCode);
            if (stopId == null) {
                _log.debug("stopId is null for stationCode: " + stationCode + " arrivalTime: " + arrivalTime);
                return null;
            }
            TreeMap<Integer, Trip> arrivalTime2TripId = stopId2ArrivalTime2TripId.get(stopId);
            if (arrivalTime2TripId == null || arrivalTime2TripId.size() == 0) return null;
            Map.Entry<Integer, Trip> floorTrip = arrivalTime2TripId.floorEntry(date);
            Map.Entry<Integer, Trip> ceilingTrip = arrivalTime2TripId.ceilingEntry(date);
            if (floorTrip == null)
                return ceilingTrip.getValue().tripId;
            if (ceilingTrip == null)
                return floorTrip.getValue().tripId;
            if (Math.abs(date - floorTrip.getKey()) > Math.abs(date - ceilingTrip.getKey())) {
                return ceilingTrip.getValue().tripId;
            } else {
                return floorTrip.getValue().tripId;
            }
        }
    }

    class Trip {
        final String tripId;

        Trip(String tripId) {
            this.tripId = tripId;
        }

        Integer startTime() {
            return tripId2StartTime.get(tripId);
        }

        boolean hasStationCode(String stationCode) {
            String stopId = stopCode2StopId.get(stationCode);
            Set<String> stationIds = tripId2StopIds.get(tripId);
            if (stopId == null || stationIds == null) return false;
            return stationIds.contains(stopId);
        }
    }

    public void consumeFile(String name, InputStreamReader stream) throws IOException, CsvValidationException {
        CSVReader csvReader = new CSVReader(stream);
        List<String> header = Arrays.asList(csvReader.readNext());
        String[] line;
        _log.debug("Processing " + name);
        switch (name) {
            case "trips.txt": {
                int routeI = header.indexOf("route_id");
                int serviceI = header.indexOf("service_id");
                int tripIdI = header.indexOf("trip_id");
                if (routeI < 0 || serviceI < 0 || tripIdI < 0) throw new CsvValidationException();
                while ((line = csvReader.readNext()) != null) {
                    Service service = services.computeIfAbsent(line[serviceI], (String k) -> new Service());
                    Route route = service.routes.computeIfAbsent(line[routeI], (String k) -> new Route());
                    String subRouteId = subrouteId(line[tripIdI]);
                    SubrouteTrips trips = route.subRouteId2Trips.computeIfAbsent(subRouteId, (String key) -> new SubrouteTrips());
                    subroute2route.put(subRouteId, line[routeI]);
                    Trip trip = new Trip(line[tripIdI]);
                    trips.trips.add(trip);
                    route.trips.put(line[tripIdI], trip);
                }
                break;
            }
            case "stop_times.txt": {
                int tripIdI = header.indexOf("trip_id");
                int arrivalTimeI = header.indexOf("arrival_time");
                int stopIdI = header.indexOf("stop_id");
                if (tripIdI < 0 || arrivalTimeI < 0) throw new CsvValidationException();
                String prevTripId = null;
                while ((line = csvReader.readNext()) != null) {
                    String tripId = line[tripIdI];
                    String stopId = line[stopIdI];
                    Integer arrivalTime = parseArrivalTime(line[arrivalTimeI]);

                    HashSet<String> tsc = tripId2StopIds.computeIfAbsent(tripId, (String key) -> new HashSet<>());
                    tsc.add(line[stopIdI]);

                    HashMap<String, ArrayList<Integer>> stopId2ArrivalTimes = tripId2StopId2ArrivalTimes.computeIfAbsent(tripId, (String key) -> new HashMap<>());
                    ArrayList<Integer> arrivalTimes = stopId2ArrivalTimes.computeIfAbsent(stopId, (String key) -> new ArrayList<>());
                    arrivalTimes.add(arrivalTime);

                    if (prevTripId == null || !prevTripId.equals(tripId)) {
                        prevTripId = tripId;
                        tripId2StartTime.putIfAbsent(tripId, arrivalTime);
                    }
                }
                break;
            }
            case "stops.txt": {
                int stopCodeI = header.indexOf("stop_code");
                int stopIdI = header.indexOf("stop_id");
                if (stopIdI < 0 || stopCodeI < 0) throw new CsvValidationException();
                while ((line = csvReader.readNext()) != null) {
                    final String stopId = line[stopIdI];
                    stopCode2StopId.computeIfAbsent(line[stopCodeI], (String key) -> stopId);
                }
                break;
            }
            case "calendar_dates.txt": {
                int dateI = header.indexOf("date");
                int serviceIdI = header.indexOf("service_id");
                if (dateI < 0 || serviceIdI < 0) throw new CsvValidationException();
                while ((line = csvReader.readNext()) != null) {
                    date2ServiceId.put(line[dateI], line[serviceIdI]);
                }
                break;
            }
        }
    }

    private Integer parseArrivalTime(String s) {
        String[] ats = s.split(":");
        int at = 0;
        at += Integer.parseInt(ats[0]) * 60 * 60;
        at += Integer.parseInt(ats[1]) * 60;
        at += Integer.parseInt(ats[2]);
        return at;
    }

    private static String subrouteId(String s) {
        String[] parts = s.split("\\|");
        if (parts.length < 3) return null;
        return parts[2];
    }

    public void onLoaded() {
        for (Service service : services.values()) {
            for (Route route : service.routes.values()) {
                for (SubrouteTrips trips : route.subRouteId2Trips.values()) {
                    for (Trip subRouteTrip : trips.trips) {
                        trips.tripStart2Trip.put(subRouteTrip.startTime(), subRouteTrip);

                        HashMap<String, ArrayList<Integer>> stopId2ArrivalTimes = tripId2StopId2ArrivalTimes.get(subRouteTrip.tripId);
                        for (String stopId : stopId2ArrivalTimes.keySet()) {
                            TreeMap<Integer, Trip> arrivalTime2TripId = trips.stopId2ArrivalTime2TripId.computeIfAbsent(stopId, (String key) -> new TreeMap<>());
                            for (Integer arrivalTime : stopId2ArrivalTimes.get(stopId)) {
                                arrivalTime2TripId.put(arrivalTime, subRouteTrip);
                            }
                        }
                    }
                }
            }
        }
    }
}
